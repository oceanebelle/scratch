# To change this template, choose Tools | Templates
# and open the template in the editor.

from utils import print_func_name_decorator
import utils
import math

__author__="oceanebelle"
__date__ ="$19-May-2012 21:39:24$"

@print_func_name_decorator
def exer_sequences(strlist, repcount):
    result = reduce(lambda x, y : x + y , strlist) * repcount
    print "Input list %s, concatenated and repeated %d: %s times " % (strlist, repcount, result)

    alphadict = dict([ (ordinal, chr(ordinal + 65)) for ordinal in range(0, 5) ])
    sorted(alphadict.keys())
    zetadict = dict([ (ordinal, chr(ordinal + 65)) for ordinal in range(21, 26) ])
    sorted(zetadict.keys())
    print "Dict 1: %s, Dict 2: %s" % (alphadict, zetadict)
    print "%d %d %s" % (len(alphadict), len(zetadict), alphadict[alphadict.keys()[0]] + zetadict[zetadict.keys()[0]])

@print_func_name_decorator
def exer_conditionals():
    fn_list = ['Jessica', 'Alice', 'Marilyn']
    for name in fn_list:
        print "%s" % name
    pass

    fn_list.extend(['Mary', 'Sheila'])

    for name in fn_list:
        if len(name) > 5:
            print "VALID: %s" % name

    heap = {'first':'alpha', 'middle':'omega', 'next':'beta', 'last':'zeta'}

    for value in heap.values():
        print "VALUE: %s" % value

@print_func_name_decorator
def exer_functions():
    def sum_array(arr_el):
        result = 0
        for num in arr_el:
            result += num
        return result

    repeat = lambda x, y : x * y

    list = [1, 2,3,4, 6, 7]
    print "SUM of %s is %d" % (list, sum_array(list))

    first = 2
    second = 2

    print "%d X %d : %d " % (first, second, repeat(first, second))

    first = 'Changed'
    print "%s X %d : %s" % (first, second, repeat(first, second))

@print_func_name_decorator
def exer_classes():
    class ExampleClass:
        def setValue(self, val):
            self._value = val
        def displaySquare(self):
            return math.sqrt(self._value)
    e = ExampleClass()
    e.setValue(9)
    print "Square of %d is %d" % (e._value, e.displaySquare())

@print_func_name_decorator
def exer_exceptions():
    def err_func():
        el = [0,1,2,8]
        print "First element: %d " % el[0]
        print "Tenth element: %d " % el[10]

    try:
        err_func()
    except LookupError, ErrMsg:
        print "%s occurred" % (ErrMsg)

def execute():
    utils.print_center(title=' EXERCISES ')
    exer_sequences(['A', 'B', 'C'], 3)
    exer_conditionals()
    exer_functions()
    exer_classes()
    exer_exceptions()
