# To change this template, choose Tools | Templates
# and open the template in the editor.
from utils import print_func_name_decorator
import utils

__author__="oceanebelle"
__date__ ="$19-May-2012 22:19:58$"

# File operations
# open input file
# open output file
# read input file
# write output file

@utils.print_func_name_decorator
def file_operations():
    fn = 'data'

    w_file = open(fn, 'w')
    try:
        lines = [chr(ordinal + 65) * 3 for ordinal in range(0,5)]
        w_file.writelines(["%s\n" % line for line in lines])
        w_file.writelines(["%d\n" % line for line in range(0,10)])
    finally:
        w_file.close()

    r_file = open(fn, 'r')
    try:
        for line in r_file:
            print "Written: %s" % line
    finally:
        r_file.close()

        

def execute():
    utils.print_center(title=' FILES ')
    file_operations()
