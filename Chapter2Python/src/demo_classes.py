# To change this template, choose Tools | Templates
# and open the template in the editor.

from utils import print_func_name_decorator, print_center

__author__="oceanebelle"
__date__ ="$20-May-2012 10:02:24$"

class Animal:
    def __init__(self, name):
        self._name = name
    def Name(self):
        return self._name

class Duck(Animal):
    def __init__(self):
        # constructor extension, call the superclass constructor
        # same goes for all other overriden methods in which you extend
        Animal.__init__(self, 'Duck')


class Person:
    _common_attr = 'TEST'

    
    def __init__(self, first_name, last_name):
        self._first_name = first_name
        self._last_name = last_name

    def FirstName(self):
        return self._first_name

    def LastName(self):
        return self._last_name

# class static method
@print_func_name_decorator
def demo_class():
    first_person = Person('Lola', 'Smith')
    second_person = Person('John', 'Doe')

    # the namespace for static variables for a class is tied to the
    # name of the class
    Person._common_attr = "CHANGE"

    p = lambda x : "[%s-%s: %s, %s]" % (x._common_attr, x.__class__, x.LastName(), x.FirstName())

    print "First person: %s " % p(first_person)
    print "Second person: %s" % p(second_person)


@print_func_name_decorator
def demo_inheritance():
    a = Duck()
    print "Name of the animal: %s" % a.Name()
    print "Is instance of Duck: %s" % isinstance(a, Duck)
    print "Is subclass of Animal: %s" % issubclass(a.__class__, Animal)

def execute():
    print_center(title="CLASS DEMO")
    demo_class()
    demo_inheritance()


    
