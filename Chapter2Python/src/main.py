# To change this template, choose Tools | Templates
# and open the template in the editor.
__author__="oceanebelle"
__date__ ="$15-May-2012 21:02:54$"

if __name__ == "__main__":
    demo_modules = [
        #'demo_string_operations',
        #'demo_sequence_operations',
        #'demo_files',
        #'demo_classes',
        #'demo_regular_expressions',
        'demo_java_integration',
        #'exercises',
    ]

    # apply __import__ method to each item in demo_modules and return the result
    # in this case the imported module object.
    demo_modules = map(__import__, demo_modules)

    for demo_module in demo_modules:
        demo_module.execute()
