import string
import random
import utils
from utils.exceptions import NotSupportedError
from collections import deque

# Define the decorator locally for multiple use
print_func_name_decorator = utils.print_func_name_decorator

__author__="oceanebelle"
__date__ ="$16-May-2012 22:38:30$"



def func_list_provider(fn):
    def list_provider(*args, **kargs):
        kargs['list'] = ['A', 'B', 'C', 'D', 'E']
        fn(*args, **kargs)
    return list_provider

def func_dict_provider(fn):
    def provider(*args, **kargs):
        kargs['d'] = {'one' : 'A', 'two' : 'B', 'three' : 'C'}
        fn(*args, **kargs)
    return provider

@func_dict_provider
@print_func_name_decorator
def dict_methods(d):
    orig = dict([ (x, y) for x, y in d.iteritems()])

    # keys
    print "Get the keys of %s : %s " % (orig, d.keys())
    # delete an entry

    del d['two']
    print "Delete entry from %s : %s" % (orig, d)

    # sort entries
    d = dict([ (x, y) for x, y in orig.iteritems() ])
    sorted(d.keys())
    print "Sorted entries from %s : %s" % (orig, d)

    # using in
    print "Is 'one' key exists in %s : %s" % (orig, 'one' in orig)
    print "Is 'ones' key exists in %s : %s" % (orig, 'ones' in orig)
    


# an unordered distinct set
@func_list_provider
@print_func_name_decorator
def list_set(list):
    list.extend(['A', 'B'])
    orig = [ el for el in list]
    print "List %s turned to set: %s" % (orig, set(list))

# list comprehensions
@func_list_provider
@print_func_name_decorator
def list_comprehensions(list):
    print "Simple comprehension %s" % ([d * 2 for d in range(0,5) ])
    print "Conditional comprehension %s" % ([d * 2 for d in range(0,5) if d % 2 == 0])
    print "Multiple comprehension %s" % ([ (x, y) for x in range(0,5) for y in range(5, 10) if x % 2 == 0 and y % 2 == 1])



# list as queues
@func_list_provider
@print_func_name_decorator
def list_queue(list):
    qlist = deque(list)
    orig = [ d for d in list]
    el = 'X'

    qlist.popleft()
    print "Dequeue %s from %s : %s" % (el, orig, qlist)

    qlist.append(el)
    print "Enqueue %s to %s : %s" % (el, orig, qlist)

# list as stacks
@func_list_provider
@print_func_name_decorator
def list_stack(list):
    orig = [ d for d in list]
    el = 'X'

    list.pop()
    print "Pop %s from %s : %s" % (el, orig, list)

    list.append(el)
    print "Append %s to %s : %s" % (el, orig, list)


# functional programming map(), reduce(), and filter()
@func_list_provider
@print_func_name_decorator
def list_func(list):
    list = list * 3

    print "Map() return of concat with X %s : %s " % (list, map(lambda x : "%sX" % x, list))

    nl = range(1, 10)
    print "Reduce() aggregates list %s : %s " % (nl, reduce(lambda x, y : x + y, nl))

    print "filter() find all A in %s : %s " % (list, filter(lambda x : x == 'A', list))

# list methods
#     sort, reverse, pop
#     insert, remove
#     extend, append
@func_list_provider
@print_func_name_decorator
def list_methods(list):
    random.shuffle(list)
    rl = [ x for x in list]
    list.sort()
    print "Sort %s : %s " % (rl, list)

    rl = [ x for x in list]
    rl.reverse()
    print "Reverse %s : %s " % (list, rl)

    rl = [ x for x in list]
    rl.pop()
    print "Pop from %s : %s " % (list, rl)

    rl = [ x for x in list]
    el = 'X'
    rl.insert(len(rl), el)
    print "Insert %s to %s : %s " % (el, list, rl)

    rl2 = [ x for x in rl]
    rl2.remove(el)
    print "Remove %s from %s : %s " % (el, rl, rl2)

    rl = [ x for x in list]
    rl.append(el)
    print "Append %s to %s : %s " % (el, list, rl)

    rl = [ x for x in list]
    rl.extend([el] * 2)
    print "Extend %s to %s : %s " % ([el] * 2, list, rl)

# sequence operations
# in, not in
# + (concatenation)
# * (shallow copy, repetition)
# s[i], s[i,j] (slice from i to j)
# s[i,j,k] (slice from i to j with step k)
# len() min() max()
# s.index(el), s.count(el) count occurrence
@func_list_provider
@print_func_name_decorator
def sequence_operations(list):
    el = 'A'
    print "Is %s in %s : %s" % (el, string.join(list), el in list)
    print "Is %s not in %s : %s" % (el, string.join(list), el not in list)
    
    list2 = list[2:3]
    print "What is %s + %s : %s" % (list, list2, list + list2)

    c = 3
    print "Repeat elements in %s, %d times : %s" % (list2, c, list2 * c)

    print "What is slice 2:4 of %s : %s" % (list, list[2:4])

    print "What is slice 1:6:2 of %s : %s" % (list, list[1:6:2])

    print "What is len, min, and max of %s : %s, %s, %s" % (list, len(list), min(list), max(list))

    list2 = list * 2
    print "What is index and count of %s in %s : %d, %d " % (el, list2, list2.index(el), list2.count(el))



def execute():
    # lists
    utils.print_center(title=' COMMON LIST OPERATIONS ')
    sequence_operations()
    list_methods()
    list_func()
    list_stack()
    list_queue()
    list_comprehensions()
    list_set()

    utils.print_center(title=' COMMON DICT OPERATIONS ')
    dict_methods()




    # dictionaries

if __name__ == "__main__":
    execute()
