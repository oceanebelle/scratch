import string
import utils
from utils.exceptions import NotSupportedError

__author__="oceanebelle"
__date__ ="$16-May-2012 22:18:29$"


# String manipulation methods

def do_join(*args):
    """
    Join strings 
    """
    return string.join(args, ', ');


def do_split(inStr, sep):
    """
    Split strings
    """
    return inStr.split(sep)

# Substring
def do_substr(inStr, start=0,end=None):
    """ Get a substring """
    return inStr[start:end]

# indexOf
def do_find(inStr, sStr):
    """ Find a string within a string """
    return inStr.find(sStr)

# Length
def do_len(inStr):
    """ Get length of a string """
    return len(inStr)

# replace
def do_replace(inStr, old, new):
    """ Replace a string within a string """
    return inStr.replace(old, new)

# repetition
def do_repetition(inStr, count):
    """ Do repetition in a string """
    return inStr * count;

# concatenation
def do_concat(*args):
    """ Concatenate strings """
    return string.join(args, '')

# string formatting
def do_format(format,*args):
    raise NotSupportedError
    # Only supported in Python 2.6
    #return string.format(format, *args)

# string validation isAlpha is alphanum, isdigit etc
def do_eval(mName, inStr, param=None):
    """ Evaluate whether a string is: isAlpha, AlphaNum, Numeric, startsWith, endsWith """
    switchCases = {
        'isAlpha'    : lambda : inStr.isalpha(),
        'isAlphaNum' : lambda : inStr.isalnum(),
        'isNumeric'  : lambda : inStr.isdigit(),
        'startsWith' : lambda : inStr.startswith(param),
        'endsWith'   : lambda : inStr.endswith(param),
    }

    defaultHandler = lambda : inStr
    return switchCases.get(mName, defaultHandler)()


def do_case(type, inStr):
    """ String case manipulation for: SWAP, TITLE, UPPER, LOWER """
    switchCases = {
        'SWAP'    : lambda : inStr.swapcase(),
        'TITLE'   : lambda : inStr.title(),
        'UPPER'   : lambda : inStr.upper(),
        'LOWER'   : lambda : inStr.lower(),
    }

    defaultHandler = lambda : inStr
    return switchCases.get(type, defaultHandler)()


def do_align(type, inStr, width, fill=' '):
    """ Adjust string horizontal orientation: LEFT, RIGHT, CENTER """
    switchCases = {
        'LEFT'    : lambda : inStr.ljust(width, fill),
        'RIGHT'   : lambda : inStr.rjust(width, fill),
        'CENTER'  : lambda : inStr.center(width, fill),
    }

    defaultHandler = lambda : inStr
    return switchCases.get(type, defaultHandler)()

def do_trim(type, inStr, pad=' '):
    """ Trim a string either: LEFT, RIGHT, BOTH """
    switchCases = {
        'LEFT'    : lambda : inStr.lstrip(pad),
        'RIGHT'   : lambda : inStr.rstrip(pad),
        'BOTH'    : lambda : inStr.strip(pad),
    }

    defaultHandler = lambda : inStr
    return switchCases.get(type, defaultHandler)()


def print_helper(func_name, method, *args, **kargs):
    """
    helper method that prints functionality, python method + actual args and the result of the
    method call.
    """
    # amalgamate all arguments and keyword arguments into one array
    all_args = list(args) + [ "%s=%s" % (k, v) for k, v in kargs.iteritems() ];

    # make sure everything is a string else string.join fails.
    all_args_str = [ str(s) for s in all_args ]

    # print the parameters and result of the call
    print '%15s: %-50s ==> %s' % (
        func_name,
        "[ %-10s (%s) ]" % (
            method.__name__,
            string.join(all_args_str, ',')),
        method(*args, **kargs))

def execute():
    utils.print_center(title=' COMMON STRING OPERATIONS ')

    print_helper("JOIN",           do_join,       'Yellow', 'Lemon', 'Tree')
    print_helper("SPLIT",          do_split,      'ONE:TWO:THREE', ':')
    print_helper("SUBSTRING",      do_substr,     'OneManShow', start=3, end=6)
    print_helper("SUBSTRING",      do_substr,     'OneManShow', start=3)
    print_helper("SUBSTRING",      do_substr,     'OneManShow', end=3)
    print_helper("SUBSTRING",      do_substr,     'OneManShow')
    print_helper("INDEXOF",        do_find,       'OneManShow', 'Man')
    print_helper("LEN",            do_len,        'OneManShow')
    print_helper("REPLACE",        do_replace,    'One1Man1Show', '1', '=')
    print_helper("REPEAT",         do_repetition, 'ONE.', 5)
    print_helper("CONCAT",         do_concat,     'one', 'TWO', 'three', 'FOUR')
    #print_helper("FORMAT",         do_format,     'one', 'TWO', 'three', 'FOUR')
    print_helper("ISALPHA",        do_eval,       'isAlpha', 'ONE')
    print_helper("ISALPHA",        do_eval,       'isAlpha', '1')
    print_helper("ISALPHANUM",     do_eval,       'isAlphaNum', 'ONE')
    print_helper("ISALPHANUM",     do_eval,       'isAlphaNum', 'ONE@')
    print_helper("ISNUM",          do_eval,       'isNumeric', 'ONE')
    print_helper("ISNUM",          do_eval,       'isNumeric', '12')
    print_helper("STARTSWITH",     do_eval,       'startsWith', 'YellowTree', param='Yell')
    print_helper("STARTSWITH",     do_eval,       'endsWith', 'YellowTree', param='ree')
    print_helper("SWAP",           do_case,       'SWAP','BOooGeYMaN')
    print_helper("TITLE",          do_case,       'TITLE', 'THE hunger GAMES')
    print_helper("UPPER",          do_case,       'UPPER', 'onetwo')
    print_helper("LOWER",          do_case,       'LOWER', 'ONETWO')
    print_helper("LEFT",           do_align,      'LEFT', 'ONE', 10, fill='*')
    print_helper("RIGHT",          do_align,      'RIGHT', 'ONE', 10, fill='*')
    print_helper("CENTER",         do_align,      'CENTER', 'ONE', 10, fill='*')
    print_helper("LTRIM",          do_trim,       'LEFT', '***ONE***', pad='*')
    print_helper("RTRIM",          do_trim,       'RIGHT', '***ONE***', pad='*')
    print_helper("TRIM",           do_trim,       'BOTH', '***ONE***', pad='*')


if __name__ == "__main__":
    execute()

