__author__="oceanebelle"
__date__ ="$15-May-2012 22:27:57$"

if __name__ == "__main__":
    print "Loaded utils"

_width = 100

def print_func_name_decorator(fn):
    """
    A method decorator which provides header printing, and a default list.
    This decorator must be the immediate outer decorator of the function.
    That is, it must be the decorator defined above the decoration and must not be
    placed on top of other decorators.
    """
    def decorator(*args, **kargs):
        print_center(title="   EXECUTING: %s   " % fn.__name__, sep='-', pre='')
        fn(*args, **kargs)
    return decorator

def print_center(title='', sep='*', pre='\n'):
    print "%s%s" % (pre, title.center(_width, sep))