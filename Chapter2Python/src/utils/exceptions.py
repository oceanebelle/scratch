import string
# To change this template, choose Tools | Templates
# and open the template in the editor.
import inspect

__author__="oceanebelle"
__date__ ="$15-May-2012 22:30:17$"

if __name__ == "__main__":
    print "Hello World"


def get_caller():
    frm = inspect.currentframe()
    frm = frm.f_back.f_back
    cde = frm.f_code
    return "%s() in %s" % (cde.co_name, cde.co_filename)

# Extend exception to create user defined exception
class NotSupportedError(Exception):
    def __init__(self):
        self.__method=get_caller()

    def __str__(self):
        return " %s needs to be implemented!" % self.__method


