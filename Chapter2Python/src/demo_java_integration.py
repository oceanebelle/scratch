# To change this template, choose Tools | Templates
# and open the template in the editor.
from utils import print_center, print_func_name_decorator

__author__="oceanebelle"
__date__ ="$20-May-2012 16:13:43$"

from javax.swing import JFrame, JOptionPane

def java_option_pane():
    result = JOptionPane.showInputDialog(None, "Please input a value:")
    print "Your input: %s" % result

def java_swing():
    fr = JFrame('title')
    fr.setSize(200, 200)
    fr.setVisible(True)

def execute():
    print_center(title="JAVA INTEGRATION")
    java_swing()

    java_option_pane()
