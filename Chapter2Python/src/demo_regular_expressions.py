# To change this template, choose Tools | Templates
# and open the template in the editor.

import re
from utils import print_func_name_decorator
import utils

__author__="oceanebelle"
__date__ ="$20-May-2012 13:37:41$"



@print_func_name_decorator
def basic_regular_expressions():
    p = 'the'
    s = 'the barley mow'
    res = re.search(p, s)

    if res:
        print "Match found."
    else:
        print "NO match"

    p = 'thes'
    res = re.search(p, s)

    if res:
        print "Match found."
    else:
        print "NO match"



# using quantifiers, characters classes
@print_func_name_decorator
def quantifiers():

    p = r'\d{3,3}-[a-zA-Z]+-[A-Z]'

    regex = re.compile(p)

    # return true if there was a match, otherwise false
    is_match = lambda x : (x and True) or False

    s_valid = '123-sEriak-J'
    print "Is %s matching %s: %s" % (s_valid, p, is_match(regex.match(s_valid)))

    s_invalid = '8948-sEriak-J'
    print "Is %s matching %s: %s" % (s_invalid, p, is_match(regex.match(s_invalid)))

# using groups and look-behinds
@print_func_name_decorator
def grouping():
    p = r'<([a-z][a-z0-9]*)>(.+)</\1>'
    regex = re.compile(p)

    # return true if there was a match, otherwise false
    is_match = lambda x : (x and True) or False
    groups = lambda y : (y and y.groups()) or None

    s_valid = r'<h1>this is a heading </h1>'
    result = regex.match(s_valid)
    print "Is %s matching %s: %s, groups: %s" % (s_valid, p, is_match(result), groups(result))

    s_invalid = r'<h1>this is a heading </h2>'
    result = regex.match(s_invalid)
    print "Is %s matching %s: %s, groups: %s" % (s_invalid, p, is_match(result), groups(result))

@print_func_name_decorator
def replace_strings():
    global num_count
    num_count = 0
    global num_total
    num_total = 0
    def process_numbers(m):
        print "Evaluating %s" % m.group(0)
        global num_count
        global num_total
        num_count += 1        
        num_total += int(m.group(0))
        return r'<no>'

    str_input = "Apples: 1, Orange: 2, Melon: 9"
    p = r'[0-9]+'
    regex = re.compile(p)

    result = regex.sub(process_numbers, str_input)

    # replacing matches using a match transformer
    print "Total: %d, Total count: %d, %s" % (num_total, num_count, result)

    # replacing matches with strings
    print "%s" % (re.sub(r'<no>', 'x', result))

@print_func_name_decorator
def split_strings():
    str_input = "Apples: 1, Orange: 2, Melon: 9"
    p = r','
    regex = re.compile(p)
    list = regex.split(str_input)
    print "Split values are: %s " % list

def execute():
    utils.print_center(title=" REGULAR EXPRESSIONS ")
    basic_regular_expressions()
    quantifiers()
    grouping()
    replace_strings()
    split_strings()